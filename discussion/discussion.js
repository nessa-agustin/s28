// CRUD operations:

/* 
    1. Create - insert
    2. Read - find
    3. Update - update
    4. Destroy - delete
*/

// Insert Method () - create documents in our DB

/* 
    Syntax:
        Insert one document:
            db.collectionName.insertOne({
                "fieldA": "valueA",
                "fieldB": "valueB",
            });

        Insert Many Documents:
            db.collectionName.insertMany([
                {
                    "fieldA": "valueA",
                    "fieldB": "valueB",
                },
                {
                    "fieldA": "valueA",
                    "fieldB": "valueB",
                }
            ]);

*/

// upon execution on Robo3T make sure not to execute your data several times


db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Dela Cruz",
    "age": 21,
    "email": "janedc@mail.com",
    "company": "none"
});


db.users.insertMany([
    {
        "firstName": "John",
        "lastName": "Santos",
        "age": 25,
        "email": "jsantos@mail.com",
        "department": "none"
    },
    {
        "firstName": "Fred",
        "lastName": "Smith",
        "age": 67,
        "email": "freds@mail.com",
        "department": "none"
    }
]);

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/

db.courses.insertMany([
    {
        "name": "Javascript 101",
        "price": 5000,
        "description": "Introduction to Javascript",
        "isActive": true
    },
    {
        "name": "HTML 101",
        "price": 2000,
        "description": "Introduction to HTML",
        "isActive": true
    },
    {
        "name": "CSS 101",
        "price": 2500,
        "description": "Introduction to CSS",
        "isActive": true
    }
]);

// Find Document / Method() - Read 
/* 
    Syntax:
        db.collectionName.find() - this will retrieve all the documents from the db
        
        db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria
        
        db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that match our criteria

        db.collectionName.findOne({}) - this will return the first document in our collection

*/

db.users.find();

db.users.find({
    "firstName": "Jane"
});

db.users.find({
    "firstName": "John",
    "age": 25
});


// Update documents/Method() - Updates our documents in our collection
/* 
    updateOne - Update the first matching document in our collection
    Syntax:
        db.collectionName.updateOne({
            "criteria": "value"
        },{
            $set : {
                "fieldToBeUpdated": "updatedValue"
            }
        }
        )

    
    updateMany() - multiple; it updates all the documents that matches our criteria

        Syntax:
            db.collectionName.updateMany(
                {
                    "criteria": "value"
                },
                {
                    $set : {
                        "fieldToBeUpdated": "updatedValue"
                    }
                }
            )
*/


db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "age": 0,
    "email": "test@mail.com",
    "department": "none"
});

// Updating One Document

db.users.updateOne(
    {
        "firstName": "Test"
    },
    {
        $set : {
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "email": "billgates@gmail.com",
            "department": "operations",
            "status": "active"
        }
    }
)


// Remove a field

db.users.updateOne(
    {
        "firstName": "Bill"
    },
    {
        $unset : {
            "status": "active"
        }
    }
)

// Updating multiple documents

db.users.updateMany(
    {
        "department": "none"
    },
    {
        $set : {
            "department": "HR"
        }
    }
)

db.users.updateOne(
    {
    },
    {
        $set : {
            "department": "Operations"
        }
    }
)



db.courses.update(
    {
        "name": "HTML 101",
    },
    {
        $set : {
            "isActive": false
        }
    }
);

db.courses.updateMany({},
    {
        $set : {
            "enrollees": 10
        }
    });


// Destroy/Delete 
/* 
    Deleting a single document
        Syntax:
            db.collectionName.deleteOne(
                {
                    "criteria": "value"
                }
            )

*/

db.users.insertOne({
    "firstName": "Test"
})

db.users.deleteOne({
    "firstName": "Test"
})

// Deleting multiple documents
/* 
    Syntax:
        db.collectionName.deleteMany(
            {
                "criteria": "value"
            }
        )

*/

db.users.deleteMany({
    "department": "HR"
})

db.courses.deleteMany({})

